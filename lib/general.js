/*
Comentario largo
Comentario largo
Comentario largo
*/
console.log(5 + 6);
var lastName = "Doe";
var lastname = "Peterson";
var y = 5;
var x = --y;

// comentario linea limple
function myFunction(p1, p2) {
    return p1 * p2;
}

// comentario linea limple
var x = myFunction(4, 3);

// comentario linea limple
function myFunction(a, b) {
    return a * b;
}

// comentario linea limple
var x = toCelsius(77);
var text = "The temperature is " + x + " Celsius";